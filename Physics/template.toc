\contentsline {chapter}{\numberline {1}Mechanics}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Constraints and Friction}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Vector Calculus and PDEs}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}Potential Energy Function, Equipotential Surfaces, Gradient}{3}{section.1.3}%
\contentsline {section}{\numberline {1.4}Conservative and Non-Conservative Forces}{3}{section.1.4}%
\contentsline {section}{\numberline {1.5}Conservation Laws}{4}{section.1.5}%
\contentsline {section}{\numberline {1.6}Non-Inertial Frames}{5}{section.1.6}%
\contentsline {section}{\numberline {1.7}Oscillations and Resonance}{5}{section.1.7}%
\contentsline {section}{\numberline {1.8}Rigid Body Motion}{5}{section.1.8}%
\contentsline {chapter}{\numberline {2}Optics}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Interference vs Diffraction}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Fraunhofer vs Fresnel Diffraction}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}Fraunhofer Diffraction}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}Diffraction Grating}{6}{section.2.4}%
\contentsline {section}{\numberline {2.5}Introduction to Polarization}{7}{section.2.5}%
\contentsline {section}{\numberline {2.6}Polarization by Reflection}{7}{section.2.6}%
\contentsline {section}{\numberline {2.7}Polarization by Double Reflection}{7}{section.2.7}%
\contentsline {section}{\numberline {2.8}Scattering of Light}{7}{section.2.8}%
\contentsline {section}{\numberline {2.9}Circular and Elliptical Polarization}{7}{section.2.9}%
\contentsline {section}{\numberline {2.10}Optical Activity}{7}{section.2.10}%
\contentsline {section}{\numberline {2.11}Basic Principles of LASER}{7}{section.2.11}%
\contentsline {section}{\numberline {2.12}Population Inversion}{8}{section.2.12}%
\contentsline {section}{\numberline {2.13}Pumping Mechanisms}{8}{section.2.13}%
\contentsline {section}{\numberline {2.14}Modes of Operation}{8}{section.2.14}%
\contentsline {section}{\numberline {2.15}Threshold Population Inversion}{8}{section.2.15}%
\contentsline {chapter}{\numberline {3}Electromagnetism and Dielectric Magnetic Properties of Materials}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Maxwell's Equations}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}Polarization}{9}{section.3.2}%
\contentsline {section}{\numberline {3.3}Permeability and Dielectric Constant}{9}{section.3.3}%
\contentsline {section}{\numberline {3.4}Polar and Non-Polar Dielectrics}{9}{section.3.4}%
\contentsline {section}{\numberline {3.5}Internal Fields in Solids}{10}{section.3.5}%
\contentsline {section}{\numberline {3.6}Clausius-Mossotti Equation}{10}{section.3.6}%
\contentsline {section}{\numberline {3.7}Applications of Dielectrics}{10}{section.3.7}%
\contentsline {section}{\numberline {3.8}Magnetization, Permeability, Susceptibility}{10}{section.3.8}%
\contentsline {section}{\numberline {3.9}Classification of Magnetic Materials}{10}{section.3.9}%
\contentsline {section}{\numberline {3.10}Ferromagnetism}{10}{section.3.10}%
\contentsline {section}{\numberline {3.11}Magnetic Domains and Hysteresis}{11}{section.3.11}%
\contentsline {section}{\numberline {3.12}Applications}{11}{section.3.12}%
\contentsline {chapter}{\numberline {4}Quantum Mechanics}{12}{chapter.4}%
\contentsline {section}{\numberline {4.1}Quantum Physics Overview}{12}{section.4.1}%
\contentsline {section}{\numberline {4.2}Black Body Radiation}{12}{section.4.2}%
\contentsline {section}{\numberline {4.3}Compton Effect}{12}{section.4.3}%
\contentsline {section}{\numberline {4.4}de Broglie Hypothesis}{12}{section.4.4}%
\contentsline {section}{\numberline {4.5}Wave-Particle Duality}{12}{section.4.5}%
\contentsline {section}{\numberline {4.6}Verification of Matter Waves}{13}{section.4.6}%
\contentsline {section}{\numberline {4.7}Uncertainty Principle}{13}{section.4.7}%
\contentsline {section}{\numberline {4.8}Schrödinger Equation}{13}{section.4.8}%
\contentsline {section}{\numberline {4.9}Particle in a Box}{13}{section.4.9}%
\contentsline {section}{\numberline {4.10}Quantum Harmonic Oscillator}{13}{section.4.10}%
\contentsline {section}{\numberline {4.11}Hydrogen Atom}{13}{section.4.11}%
\contentsline {chapter}{\numberline {5}Statistical Mechanics}{14}{chapter.5}%
\contentsline {section}{\numberline {5.1}Macrostate and Microstate}{14}{section.5.1}%
\contentsline {section}{\numberline {5.2}Density of States}{14}{section.5.2}%
\contentsline {section}{\numberline {5.3}Maxwell-Boltzmann Statistics}{14}{section.5.3}%
\contentsline {section}{\numberline {5.4}Fermi-Dirac Statistics}{14}{section.5.4}%
\contentsline {section}{\numberline {5.5}Bose-Einstein Statistics}{15}{section.5.5}%
\contentsfinish 
