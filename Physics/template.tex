\documentclass{report}

\input{preamble}
\input{macros}
\input{letterfonts}

\title{\Huge{Physics}\\1st Semester Notes}
\author{\huge{Raif Salauddin Mondal}}
\date{23/01/2024}

\begin{document}

\maketitle
\newpage% or \cleardoublepage
% \pdfbookmark[<level>]{<title>}{<dest>}
\pdfbookmark[section]{\contentsname}{toc}
\tableofcontents
\pagebreak

\chapter{Mechanics}
\nt{Problems including constraints \& friction. Basic ideas of vector calculus and partial differential equations. Potential energy function $F = -grad V$, equipotential surfaces and meaning of gradient. Conservative and non-conservative forces. Conservation laws of energy \& momentum.Non-inertial frames of reference. Harmonic oscillator; Damped harmonic motion forced oscillations and resonance. Motion of a rigid body in a plane and in 3D. Angular velocity vector. Moment of inertia.}
\section{Constraints and Friction}
Constraints limit degrees of freedom and alter dynamics. Friction opposes relative motion between surfaces.
\ex{}{Pendulum pivot, sliding block, pulleys. Friction reduces velocities.}
\section{Vector Calculus and PDEs}
\begin{itemize}
\item Vector calculus describes multi-variable functions and fields. Key tools are gradients, curl, divergence.
\item PDEs relate multivariable function rates of change. Used to formulate physical system dynamics.
\item \textbf{Applications}: Fluid flow, electromagnetics, heat transfer problems.
\end{itemize}
\section{Potential Energy Function, Equipotential Surfaces, Gradient}
\begin{itemize}
\item Potential energy function V(x,y,z) gives potential energy in a conservative field.
\item Equipotential surfaces are contours of constant V. Gradient points in direction of steepest ascent.
\item Gradient $\nabla V$ = force F, since F causes maximum potential energy change.
\end{itemize}
\section{Conservative and Non-Conservative Forces}
\begin{itemize}
\item Conservative forces depend only on position. Work done is path-independent. Gravity, spring forces.
\item Non-conservative depend on velocity. Work is path-dependent. Friction, air drag.
\end{itemize}
\section{Conservation Laws}
\begin{itemize}
\item Energy conserved in closed systems. Total initial energy = total final energy.
\item Momentum conserved when external forces absent. Total initial momentum = total final momentum.
\item Allows predicting system dynamics from initial states.
\end{itemize}
\pagebreak
\section{Non-Inertial Frames}
\begin{itemize}
\item Accelerating or rotating frames. Objects appear to experience fictitious forces.
\ex{}{Centrifugal, Coriolis forces. Must account for frame acceleration.}
\end{itemize}
\section{Oscillations and Resonance}
\begin{itemize}
\item Harmonic oscillators have restoring force proportional to displacement. Sinusoidal motion.
\item Damping reduces amplitude over time. Forced oscillations can drive resonance at natural frequency.
\end{itemize}
\section{Rigid Body Motion}
\begin{itemize}
\item Planar motion involves translation and rotation. 3D motion more complex.
\item Angular velocity vector gives instantaneous axis and speed of rotation.
\item Moment of inertia relates rotational kinetic energy and angular momentum. Depends on mass distribution.
\end{itemize}
\pagebreak
\chapter{Optics}
\nt{\begin{itemize}
\item Distinction between interference and diffraction, Fraunhofer and Fresnel diffraction, Fraunhofer diffraction at single slit, double slit, and multiple slits (only the expressions for max;min, \& intensity and qualitative discussion of fringes); diffraction grating(resolution formula only), characteristics of diffraction grating and its applications.
\item Polarisation : Introduction, polarisation by reflection, polarisation by double reflection, scattering of light, circular and elliptical polarisation, optical activity.
\item Lasers: Principles and working of laser : population inversion, pumping, various modes, threshold population inversion with examples.
\end{itemize}}
\section{Interference vs Diffraction}
\begin{itemize}
\item Interference involves the superposition of waves from two or more coherent sources.
\item Diffraction is the bending of waves around obstacles and openings comparable to the wavelength.
\end{itemize}
\section{Fraunhofer vs Fresnel Diffraction}
\begin{itemize}
\item Fraunhofer diffraction occurs when waves are observed at a large distance from the obstacle/aperture.
\item Fresnel diffraction occurs when waves are observed at a closer distance to the obstacle/aperture.
\end{itemize}
\section{Fraunhofer Diffraction}
\begin{itemize}
\item \textbf{Single slit} - Intensity pattern has sine squared distribution. Minima at $\theta m = \frac{m \lambda}{b}$, maxima between.
\item \textbf{Double slit} - Fringes based on path difference. Minima at $dsin\theta = m\lambda$, maxima between.
\item \textbf{Multiple slits} - More slits give sharper principal maxima and narrower fringes.
\end{itemize}
\section{Diffraction Grating}
\begin{itemize}
\item \textbf{Resolution formula} - $R = \frac{\lambda}{d}$, where d is slit spacing. Finer gratings give higher resolution.
\item \textbf{Characteristics} - Periodic slit spacing creates constructive interference at specific angles.
\item \textbf{Applications} - Dispersive element in spectrometers, filtering and selection of wavelengths.
\end{itemize}
\section{Introduction to Polarization}
\begin{itemize}
\item \textbf{Definition}: Restriction of the vibrations of a light wave to a single plane.
\item \textbf{Nature of Light}: Light waves consist of oscillating electric and magnetic fields. Can be polarized by filtering.
\item \textbf{Polarization States}: Linear, circular, elliptical based on orientation of the oscillating fields.
\end{itemize}
\section{Polarization by Reflection}
\begin{itemize}
\item \textbf{Mechanism}: Reflected light polarized perpendicular to plane of incidence due to selective attenuation of parallel component.
\item \textbf{Angle Dependence}: Degree of polarization varies with angle of incidence. Maximized at Brewster's angle.
\item \textbf{Applications}: Stress analysis, sunglasses, photography.
\end{itemize}
\section{Polarization by Double Reflection}
\begin{itemize}
\item \textbf{Principle}: Two successive reflections polarize non-polarized light. Reflections must be parallel.
\item \textbf{Devices}: Nicol prism, Glan-Thompson prism, Wollaston prism.
\item \textbf{Advantages}: Simple method. Limitations - Angular, wavelength dependence.
\end{itemize}
\section{Scattering of Light}
\begin{itemize}
\item \textbf{Scattering}: Light forced to deviate during interactions with particles in a medium.
\item \textbf{Tyndall Effect}: Scattering of light by colloidal particles or gas molecules causes bluish hue.
\item \textbf{Significance}: Explains colors of sky, fog, opalescence. Enables visibility of beams.
\end{itemize}
\section{Circular and Elliptical Polarization}
\begin{itemize}
\item \textbf{Generation}: Combination of two perpendicular linearly polarized components with phase difference.
\item \textbf{Applications}: Chiral spectroscopy, optics testing, 3D displays, fiber optics.
\item \textbf{Comparison}: Circular - constant amplitude, rotating orientation. Elliptical - varying amplitude.
\end{itemize}
\section{Optical Activity}
\begin{itemize}
\item \textbf{Definition}: Rotation of linearly polarized light as it passes through certain materials.
\item \textbf{Chirality}: Due to chiral molecular structure lacking mirror symmetry.
\item \textbf{Measurement}: Polarimeter. \emph{Applications: Chiral compound analysis, glucose sensing.}
\end{itemize}
\section{Basic Principles of LASER}
\begin{itemize}
\item \textbf{Definition}: LASER - Light Amplification by Stimulated Emission of Radiation.
\item \textbf{Stimulated Emission}: Incident photon triggers release of identical photon from excited atom.
\item \textbf{Energy States}: Atoms excited to higher energy levels, decay to lower levels by emitting photons.
\end{itemize}
\section{Population Inversion}
\begin{itemize}
\item \textbf{Definition}: Higher population of atoms in excited state than ground state.
\item \textbf{Achieving}: Optical or electrical pumping, collisions, irreversible transitions.
\item \textbf{Importance}: Necessary for light amplification through stimulated emission.
\end{itemize}
\section{Pumping Mechanisms}
\begin{itemize}
\item \textbf{Definition}: Process of exciting atoms to higher energy levels to create population inversion.
\item \textbf{Types}: Optical (using light sources), electrical (applying voltage), chemical reactions, collisions.
\item \textbf{Energy Transfer}: Atoms absorb pump energy and get excited to higher states.
\end{itemize}
\section{Modes of Operation}
\begin{itemize}
\item \textbf{Continuous Wave}: Constant output beam. Used for welding, cutting.
\item \textbf{Pulsed}: Short bursts of high peak power. Used for material processing, LIDAR, medicine.
\item \textbf{Q-Switching}: Generates high energy pulses by modulating losses. Used for range-finding.
\end{itemize}
\section{Threshold Population Inversion}
\begin{itemize}
\item \textbf{Definition}: Minimum population inversion density required to achieve lasing.
\item \textbf{Importance}: Compensates for resonator losses before amplification and lasing occur.
\ex{}{Gas lasers may require very high threshold inversion compared to diode lasers.}
\end{itemize}
\pagebreak
\chapter{Electromagnetism and Dielectric Magnetic Properties of Materials}
\section{Maxwell's Equations}
\begin{itemize}
\item \textbf{Overview}: Set of four partial differential equations describing electromagnetic fields and their interactions. Relate E, B, electric charges, and currents.
\item \textbf{Applications}: Form basis for electromagnetism, optics, electronics. Used to model EM wave propagation, transmission lines, antennas.
\end{itemize}
\section{Polarization}
\begin{itemize}
\item \textbf{Definition}: Separation of positive and negative charges in a dielectric material under applied E-field.
\item \textbf{Types}: Electronic - displacement of electrons relative to nuclei. Ionic - displacement of charged ions.
\item \textbf{Effects}: Induced dipole moments which oppose and reduce the E-field in matter. Influences dielectric behavior.
\end{itemize}
\section{Permeability and Dielectric Constant}
\begin{itemize}
\item \textbf{Permeability}: Ability of a material to support formation of magnetic field within itself.
\item \textbf{Dielectric Constant}: Measure of electrical polarization of a material by an applied E-field.
\item \textbf{Relationship}: Depends on material composition, structure, bonding. Affects response to EM fields.
\end{itemize}
\section{Polar and Non-Polar Dielectrics}
\begin{itemize}
\item \textbf{Definition}: Polar - materials with permanent dipoles. Non-polar - materials without permanent dipoles.
\ex{}{Polar - $H_{2}O$, $NH_{3}$, ethanol. Non-polar - benzene, carbon tetrachloride.}
\item \textbf{Applications}: Polar used where stronger polarization needed. Non-polar for temperature stability.
\end{itemize}
\section{Internal Fields in Solids}
\begin{itemize}
\item \textbf{Generation}: Result from atomic/molecular forces and applied external fields.
\item \textbf{Effects}: Influence charge transport and overall electrical characteristics.
\item \textbf{Applications}: Field emission, electro-optic effects, ferroelectric devices.
\end{itemize}
\section{Clausius-Mossotti Equation}
\begin{center}
$\alpha = \frac{\frac{3}{4}\pi N}{\frac{\varepsilon - 1}{\varepsilon - 2}}$\\
\end{center}
where N is the number of molecules per unit volume.
\section{Applications of Dielectrics}
\begin{itemize}
\item \textbf{Capacitors}: Used as insulator between plates to increase capacitance.
\item \textbf{Insulation}: Electrical insulation in cables, transformers, motors.
\item \textbf{Electronic devices}: Integrated circuits, printed circuit boards, packaging.
\end{itemize}
\section{Magnetization, Permeability, Susceptibility}
\begin{itemize}
\item \textbf{Magnetization (M)}: extent of magnetic dipole moments per unit volume in a material.
\item \textbf{Permeability ($\mu$)}: ability of a material to support formation of a magnetic field within itself.
\item \textbf{Susceptibility ($\chi$)}: degree of magnetization of a material in response to an applied magnetic field.
\end{itemize}
\cor{Related by}{$M = \chi H, \mu = \mu \theta(1 + \chi)$}
\section{Classification of Magnetic Materials}
\begin{itemize}
\item \textbf{Ferromagnetic}: Strongly attracted to magnetic fields. High permeabilities. Iron, cobalt, nickel.
\item \textbf{Antiferromagnetic}: Adjacent atomic dipoles aligned antiparallel. Iron manganese alloys.
\item \textbf{Paramagnetic}: Weakly attracted to magnetic fields. Positive susceptibility. Aluminum, platinum.
\item \textbf{Diamagnetic}: Repelled from magnetic fields. Negative susceptibility. Copper, gold, quartz.
\end{itemize}
\section{Ferromagnetism}
\begin{itemize}
\item \textbf{Explanation}: Strong parallel alignment of magnetic moments. Caused by quantum mechanical exchange interactions.
\item \textbf{Curie Temperature}: Transition temperature above which material becomes paramagnetic.
\item \textbf{Behavior}: Non-linear response to applied field. Saturation magnetization. Hysteresis.
\end{itemize}
\section{Magnetic Domains and Hysteresis}
\begin{itemize}
\item \textbf{Domains}: Regions of uniform magnetization. Cause net magnetization to be zero without external field.
\item \textbf{Hysteresis}: Lag between changing field and magnetization due to domain wall pinning. Energy loss.
\item \textbf{Influencing Factors}: Impurities, grain structure, temperature, mechanical stress.
\end{itemize}
\section{Applications}
\begin{itemize}
\item \textbf{Electromagnetic devices}: transformers, motors, relays, inductors, sensors.
\item \textbf{Magnetic storage}: hard disk drives, magnetic tape.
\item \textbf{Biomedical}: MRI, magnetically targeted drug delivery, biosensors.
\end{itemize}
\pagebreak
\chapter{Quantum Mechanics}
\nt{Introduction to quantum physics, black body radiation, explanation using the photon concept,Compton effect, de Broglie hypothesis, wave-particle duality, verification of matter waves,uncertainty principle, Schrodinger wave equation, particle in box, quantum harmonic oscillator,hydrogen atom.}
\section{Quantum Physics Overview}
\begin{itemize}
\item \textbf{Definition}: Theory describing nature and behavior of matter and energy at atomic and subatomic scale.
\item \textbf{Key Concepts}: Quantization, wave-particle duality, probability, measurement.
\end{itemize}
\section{Black Body Radiation}
\begin{itemize}
\item \textbf{Concept}: Electromagnetic radiation emitted by heated objects.
\item \textbf{Photon Concept}: Emission/absorption of radiation in discrete quanta explained it.
\item \textbf{Applications}: Understanding stellar emissions, LEDs, lasers.
\end{itemize}
\section{Compton Effect}
\begin{itemize}
\item \textbf{Explanation}: Scattering of photons by electrons shows particulate nature of light.
\item \textbf{Photon Concept}: Explained shift in wavelength after collision.
\item \textbf{Significance}: Definitive evidence of the photon.
\end{itemize}
\section{de Broglie Hypothesis}
\begin{itemize}
\item \textbf{Statement}: Matter has wave-like properties. Wavelength $\lambda =\frac{h}{p}.$
\item \textbf{Evidence}: Electron diffraction and interference experiments.
\item \textbf{Implications}: Wave-particle duality, basis of quantum mechanics.
\end{itemize}
\section{Wave-Particle Duality}
\begin{itemize}
\item \textbf{Definition}: Fundamental particles exhibit both wave and particle properties.
\item \textbf{Experiments}: Double slit, photoelectric effect, Compton scattering.
\item \textbf{Interpretation}: Challenging but central to quantum theory.
\end{itemize}
\section{Verification of Matter Waves}
\begin{itemize}
\item \textbf{Evidence}: Electron diffraction and interference experiments.
\item \textbf{Interference Patterns}: Observed due to wave-like nature of electrons.
\item \textbf{Applications}: Electron microscopy, crystallography.
\end{itemize}
\section{Uncertainty Principle}
\begin{itemize}
\item \textbf{Statement}: Cannot know particle's position and momentum simultaneously beyond a limit.
\item \textbf{Interpretation}: Fundamental limit, not just measurement limitation.
\item \textbf{Examples}: Position-momentum tradeoff in spectroscopy.
\end{itemize}
\section{Schrödinger Equation}
\begin{itemize}
\item \textbf{Derivation}: Wave equation derived from de Broglie relation.
\item \textbf{Wavefunction}: Mathematical description of the quantum state.
\item \textbf{Applications}: Atoms, molecules, condensed matter, nano particles.
\end{itemize}
\section{Particle in a Box}
\begin{itemize}
\item \textbf{Concept}: Simplest quantum system - particle confined in a box.
\item \textbf{Quantization}: Only specific discrete energies allowed.
\item \textbf{Eigenfunctions}: Wavefunctions of the allowed states.
\end{itemize}
\section{Quantum Harmonic Oscillator}
\begin{itemize}
\item \textbf{Description}: Model for vibrational motion in quantum physics.
\item \textbf{Energy Levels}: Equally spaced quantized levels.
\item \textbf{Applications}: Atoms, molecules, nuclei, quantum dots.
\end{itemize}
\section{Hydrogen Atom}
\begin{itemize}
\item \textbf{Bohr Model}: Electron orbiting nucleus in allowed orbits.
\item \textbf{Quantum Numbers}: Principal, azimuthal, magnetic.
\item \textbf{Schrödinger Solution}: Gives precise energetics and probabilities.
\end{itemize}
\pagebreak
\chapter{Statistical Mechanics}
\nt{Macrostate, Microstate, Density of states, Qualitative treatment of Maxwell Boltzmann, Fermi-Dirac and Bose-Einstein statistics.
Do the same prompt engineering for this topic as well}
\section{Macrostate and Microstate}
\begin{itemize}
\item \textbf{Definition}: Macrostate describes overall system properties. Microstate specifies precise configurations of all particles.
\item \textbf{Relationship}: Many possible microstates yield the same macrostate.
\ex{}{Gas in a container - macrostate is volume, pressure and temperature. Microstates are individual molecule positions and velocities.}
\end{itemize}
\section{Density of States}
\begin{itemize}
\item \textbf{Definition}: Number of available quantum states per interval of energy for a system.
\item \textbf{Role}: Determines number of ways particles can occupy available states and their distribution.
\item \textbf{Applications}: Fermi-Dirac and Bose-Einstein statistics, semiconductor physics.
\end{itemize}
\section{Maxwell-Boltzmann Statistics}
\begin{itemize}
\item \textbf{Principles}: Applicable for non-interacting particles. Distribution of particles in energy levels given by Maxwell-Boltzmann equation.
\item \textbf{Speed Distribution}: Most probable speed proportional to square root of temperature. Distribution broadens with temperature.
\item \textbf{Temperature Dependence}: Average energy and higher energy states increase with temperature.
\end{itemize}
\section{Fermi-Dirac Statistics}
\begin{itemize}
\item \textbf{Introduction}: Applies to fermions due to Pauli exclusion principle. Determines occupancy of energy levels.
\item \textbf{Fermi Energy}: Energy of highest occupied level at absolute zero temperature.
\item \textbf{Applications}: Electrons in metals and semiconductors, white dwarfs.
\end{itemize}
\section{Bose-Einstein Statistics}
\begin{itemize}
\item \textbf{Introduction}: Applies to bosons which can fully occupy energy states. Determines distribution.
\item \textbf{Bose-Einstein Condensation}: Macroscopic occupation of lowest energy state below a critical temperature.
\ex{}{Photons, superfluid helium-4, superconductivity.}
\end{itemize}
\end{document}
